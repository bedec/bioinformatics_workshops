#!/usr/bin/env nextflow

///////////// params ////////////////

params.infi='file/txt'
params.home='/home/nick/'
params.spBin='/usr/bin'

accs=Channel
	.fromPath( "${params.infi}" )
	.splitText() { it.trim() }



process download {
	errorStrategy 'ignore'
	tag { acc }
	//scratch true
	executor 'local'
	maxForks 2
	input:
	val(acc) from accs

	output:
	set val(acc), file("${acc}/*.fastq.gz") into downloads

	script:
	l="${acc}"
	l1=l[0..5]
	"""
	/well/bag/nicholas/soft/sratoolkit.2.10.8-centos_linux64/bin/prefetch ${acc} 
	/well/bag/nicholas/soft/sratoolkit.2.10.8-centos_linux64/bin/fasterq-dump -O ${acc} ${acc} 
	gzip ${acc}/*.fastq
	"""

}

process spades {
	errorStrategy 'ignore'
	tag { acc }
	scratch true
	cpus 4
	memory '16GB'
	executor 'sge'
	penv 'shmem'

	clusterOptions '-S /bin/bash -P bag.prjc -q short.qc -cwd -V -pe shmem 4'

	publishDir 'contigs', mode: 'copy', overwrite: true

	input:
	set val(acc), file("${acc}/*.fastq.gz") from downloads

	output:
	file("${acc}_contigs.fasta") into contigs

	script:
	"""
	${params.spBin}/spades.py -1 "${acc}/1.fastq.gz" -2 "${acc}/2.fastq.gz" \
	-o ${acc}_contigs -t ${task.cpus}
	cp ${acc}_contigs/contigs.fasta ${acc}_contigs.fasta
	"""

}
