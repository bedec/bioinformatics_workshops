# High performance computing with RESCOMP

## Introduction to RESCOMP

* What is HPC? What is rescomp/BMRC?
* Why might I need HPC?
  * Usually because something you are running on your laptop/workstation is taking FAR too long
	* Might also enable you to do something you otherwise could NOT do. e.g. a parameter sweep
	* Or the software or compiler might require a licence and you don’t have it
	* The data you want to analyse is too big to move around (‘take the compute to the data’)
* How do I access rescomp?
* How do I run a job on rescomp? Why is it so confusing?
* How can I really annoy the sysadmins and everyone else in MMM?
* How do I download files from rescomp?

### Logging into RESCOMP

`$ ssh <username>@rescomp.well.ox.ac.uk`

### Example BASH job

```
#$ -S /bin/bash
#$ -N mmm-1
#$ -e mmm-1.err
#$ -o mmm-1.out
#$ -P bag.prjc
#$ -q short.qc -cwd -V

echo "hello MMM!"
```

```
$ qsub rescomp-bash-test.sh
$ qstat
job-ID     prior   name       user         state submit/start at     queue                          jclass                         slots ja-task-ID
------------------------------------------------------------------------------------------------------------------------------------------------
  29930414 0.00000 mmm-2      pfowler      qw    11/03/2020 14:44:22                                                                   1        
```

### How do I delete the above job before it finishes?

```
$ qdel 29930414
```

### How do I delete all my jobs?

This will delete ALL the jobs you currently have in the queue!

```
for i in `qstat | grep pfowler | awk '{​​print $1}​​' `; do qdel $i ;done
```

### How do I find out how much disc space is left (that we all share!)

```
$ df -BG /well/bag/
Filesystem     1G-blocks    Used Available Use% Mounted on
WTGPFS2          110000G 101509G     8492G  93% /gpfs2
```



## Using Nextlow with RESCOMP
I have a nextflow binary in my home folder, so these exact commands might not work for you.


```bash
~/nextflow run main.nf --infi accessions.txt \
	 -resume \
	--spBin /gpfs0/apps/well/SPAdes/3.6.0/bin/ \
	--home /users/bag/clme1715 \
	-with-trace \
	-with-report spades.html
```
