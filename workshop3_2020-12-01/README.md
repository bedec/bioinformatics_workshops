# Basic COVID analysis

We're going to go through the ARTIC COVID analysis pipeline to generate alignment BAM files and consensus sequences. 
We're going to look at fing out 

- total number of reads per run and per barcode
- number of human reads
- number of reads of target of interest
- % genome covered at x1 and eg x20
- number of Ns

We're also going to produce some plots such as:
- coverage by genome position
- summary statistics of coverage - either as an absolute % of genome at x1,x20,x30 coverage etc. or coverage by amount of data generated (number of reads/mbp data/time)

I've written a seperate repo about running the artic pipeline on MMM infrastructure here:
https://gitlab.com/ModernisingMedicalMicrobiology/bioinformatics_mmm_covid


## Total number of reads per run and per barcode
There are a number of ways to do this. One very quick and not always recommended way to do this is to use `wc -l` for example:

bash
```bash
zcat /mnt/microbio/Nanopore/COVID_ARTIC_2/basecalled_fastq/barcode01_r0barcode01b* | wc -l
# output should be 1974788
```

This gives us the number of **bold** for the file. There are normally 4 lines per reads, so we can divide by 4.

bash
```bash
reads=$((`zcat /mnt/microbio/Nanopore/COVID_ARTIC_1/basecalled_fastq/barcode01_r0barcode01b* | wc -l`/ 4))
echo reads
# output should be 2419
``` 

The problem with this approact is some software create fastq files with line breaks, so there are more than four lines per read.

My preferred method with Nanopore data is to look at the sequencing summary file from the gridion. Here is an example with a shortened sequencing summary file where I use pandas in Python to count the number of reads per barcode that pass or fail quality filtering.

python3
```python
import pandas as pd                                                                                                        

df=pd.read_csv('data/sequencing_summary_FAO14353_bfe31b14.txt',sep='\t')                                                   
df.columns                                                                                                                 
# Index(['filename_fastq', 'filename_fast5', 'read_id', 'run_id', 'channel',
#        'mux', 'start_time', 'duration', 'num_events', 'passes_filtering',
#        'template_start', 'num_events_template', 'template_duration',
#        'sequence_length_template', 'mean_qscore_template',
#        'strand_score_template', 'median_template', 'mad_template', 'pore_type',
#        'experiment_id', 'sample_id', 'end_reason', 'barcode_arrangement',
#        'barcode_full_arrangement', 'barcode_kit', 'barcode_variant',
#        'barcode_score', 'barcode_front_id', 'barcode_front_score',
#        'barcode_front_refseq', 'barcode_front_foundseq',
#        'barcode_front_foundseq_length', 'barcode_front_begin_index',
#        'barcode_rear_id', 'barcode_rear_score', 'barcode_rear_refseq',
#        'barcode_rear_foundseq', 'barcode_rear_foundseq_length',
#        'barcode_rear_end_index'],
#       dtype='object')

g=df.groupby(['barcode_arrangement','passes_filtering'])['read_id'].count()                                                
# barcode_arrangement  passes_filtering
# barcode01            False                 24
#                      True                1433
# barcode02            False                 46
#                      True                2921
# barcode03            False                 61
#                      True                3548
# barcode04            False                  1
#                      True                   3
# barcode05            False                 18
#                      True                 935
# barcode06            True                   1
# barcode07            True                   1
# unclassified         False                679
#                      True                 328
# Name: read_id, dtype: int64
```

We can also use this method to get some summary statistics such as median read lenths and qualities

python3
```python
b=df.groupby(['barcode_arrangement','passes_filtering'])['sequence_length_template'].describe()
print(b)
# barcode_arrangement passes_filtering   count        mean         std    min     25%    50%     75%     max 
# barcode01           False               24.0  463.708333  104.114268  274.0  428.50  445.0  462.25   851.0
#                     True              1433.0  457.920447  120.157610  178.0  433.00  446.0  482.00  1589.0
# barcode02           False               46.0  468.413043   93.908366  251.0  428.25  470.0  503.00   841.0
#                     True              2921.0  473.387196  136.928357  171.0  436.00  453.0  499.00  2527.0
# barcode03           False               61.0  469.836066  106.219612  255.0  415.00  453.0  493.00   873.0
#                     True              3548.0  475.622886  100.553012  171.0  439.00  462.0  498.00  1858.0
# barcode04           False                1.0  468.000000         NaN  468.0  468.00  468.0  468.00   468.0
#                     True                 3.0  701.000000   71.582121  651.0  660.00  669.0  726.00   783.0
# barcode05           False               18.0  458.444444   54.422338  361.0  426.25  472.0  490.25   574.0
#                     True               935.0  482.783957   95.987432  171.0  443.00  488.0  507.00  1776.0
# barcode06           True                 1.0  489.000000         NaN  489.0  489.00  489.0  489.00   489.0
# barcode07           True                 1.0  842.000000         NaN  842.0  842.00  842.0  842.00   842.0
# unclassified        False              679.0  434.830633  200.110300   79.0  375.50  431.0  477.50  3293.0
#                     True               328.0  450.878049  126.947883  171.0  422.00  440.5  461.00  1508.0
```

We can also easily plot with seaborn

python3
```python
import seaborn as sns
ax=sns.boxplot(x='barcode_arrangement',y='sequence_length_template',hue='passes_filtering',data=df)
```
This should produce an image line the one below
![Alt-Text](figs/read_length_boxplot.png)

We can also look at yield over time fairly easily with this file

python3
```python
# sort by start time
df=df.sort_values(by='start_time',ascending=True)
# calculate cumulative yeild of bases with cumsum()
df['Yield (bases)']=df['sequence_length_template'].cumsum()
# Create a column of time in minute, for longer runs you can do the same with hours
df['Time from start (minutes)']=df['start_time']/60
# subsample to 500 datapoints, we have time and yield so this will make plotting way faster!
df2=df.sample(500)
# plot and save
ax=sns.lineplot(x='Time from start (minutes)',y='Yield (bases)',data=df2)
plt.savefig('yield_over_time.png')
```
That should produce a plot like this
![Alt-Text](figs/yield_over_time.png)

## Number of human reads
To find out the number of human reads, we can use centrifuge with an appropriate database. The commands for centrifuge on analysis1 would be like so:

bash
```bash
# variables for centrifuge database (cdb) and fastq files (fqs) not barcode01 only
cdb=/mnt/microbio/HOMES/nick/dbs/centrifuge/centrifuge_2019-09-13/centrifuge_2019-09-24
fqs=/mnt/microbio/Nanopore/COVID_SISPA_1/basecalled_fastq/barcode01_r0barcode01b

# centrifuge only takes files in comma seperated lists annoyingly
data=`ls -m ${fqs}* | tr -d '\\n' | tr -d ' '`                   

# this is the standard centrifuge command, it can take a while to run        
centrifuge -f -x $cdb \
    --mm -q -U $data \
    -S cent.tsv --min-hitlen 16 -k 1  
```
Note how we're using `--min-hitlen 16` so the error profile shouldn't prevent too many matches with the database

Then create a report using this command.

bash
```bash
centrifuge-kreport \                                                        
    --min-score $thresh \                                                   
    -x $cdb cent.tsv > report.txt     

# we can visualise the report file with less, or just grep for humans
grep 'Homo sapiens' report.txt
# the output should show us 9818 reads
```

This is all run automatically in crumpit anyway so can be viewed on the crumpit manager webpage https://crumpitmanager.mmmoxford.uk
![Alt-Text](figs/crumpit_manager_screenshot.png)

## number of reads of target of interest
The ARTIC pipeline will produce a bam file with the nanopore reads aligned to the nCOV-2019 reference genome. From this we can calculate the number of reads that map to the target of interest. 

bash
```bash
samtools view -c barcode01.sorted.bam
# output 16232
# or just primary alignments (useful for other projects) 
samtools view -c -F 260 SAMPLE.bam
# also output 16232
```

## % genome covered at x1 and eg x20
How do we calculate coverage over the genome? samtools depth is useful here;

bash
```bash
samtools depth -h
Usage: samtools depth [options] in1.bam [in2.bam [...]]
Options:
   -a                  output all positions (including zero depth)
   -a -a (or -aa)      output absolutely all positions, including unused ref. sequences
   -b <bed>            list of positions or regions
   -f <list>           list of input BAM filenames, one per line [null]
   -l <int>            read length threshold (ignore reads shorter than <int>) [0]
   -d/-m <int>         maximum coverage depth [8000]. If 0, depth is set to the maximum
                       integer value, effectively removing any depth limit.
   -q <int>            base quality threshold [0]
   -Q <int>            mapping quality threshold [0]
   -r <chr:from-to>    region
      --input-fmt-option OPT[=VAL]
               Specify a single input file format option in the form
               of OPTION or OPTION=VALUE
      --reference FILE
               Reference sequence FASTA FILE [null]

The output is a simple tab-separated table with three columns: reference name,
position, and coverage depth.  Note that positions with zero coverage may be
omitted by default; see the -a option.
```
So we can run it like this to get all the positions into a csv file:

bash
```bash
samtools depth -aa barcode01.sorted.bam > barcode01_depth.tsv

head barcode01_depth.tsv
# MN908947.3	1	173
# MN908947.3	2	179
# MN908947.3	3	179
# MN908947.3	4	185
# MN908947.3	5	206
# MN908947.3	6	205
# MN908947.3	7	210
# MN908947.3	8	215
# MN908947.3	9	216
# MN908947.3	10	218
```

Then we can use pandas (or R) to calucalte the coverage over the genome.

python3
```python
import pandas as pd

df=pd.read_csv('data/barcode01_depth.tsv',names=['chrom','pos','depth'],sep='\t')

# number of positions with coverage above 1
x1 = df['pos'][df['depth']>=1].count()
x1_per= (x1/df['pos'].max())*100
print(x1_per)                                                                                                  
# 99.90301976390329

# Same for coverage above 20
x20 = df['pos'][df['depth']>=20].count()
x20_per= (x20/df['pos'].max())*100
print(x20_per)                                                                                                  
# 99.80269538173428
```

We can then also use pandas (or R), to visualise the coverage.

python3
```python
import pandas as pd

df=pd.read_csv('data/barcode01_depth.tsv',names=['chrom','pos','depth'],sep='\t')
ax=sns.lineplot(x='pos', y='depth',data=df) 
plt.show()
# or
plt.savefig('coverage_depth.png')
```

This will produce a plot like the one below:
![Alt-Text](figs/coverage_depth.png)
