# Workshop 0, how to draw a phylogenetic tree with Python3 and ete3

We're going to draw a phylogenetic tree from a newick file and add annotations to give us extra information. You can find more information about ete3 here (http://etetoolkit.org/docs/latest/tutorial/index.html). The newick file has already been made from IqTree and clonalframeml, using David Eyre's script found here (https://github.com/davideyre/runListCompare).

I need to do this in order to see if some Staph genomes we recovered from a culture negative PJI sample was from barcode contamination from the same run, or a genuinely different genome that didn't get picked up from lab culture. 

## Installation

I'm going to assume you have already installed ete3 on your system, getting stuff installed is a pain and could take a lot of time to install and every different machine type. It is installed on analysis1. Let me know if you have installation issues at the end of workshop.

You can download this repository and change directory to this workshop like so:
```bash
git clone https://gitlab.com/ModernisingMedicalMicrobiology/bioinformatics_workshops.git
cd ModernisingMedicalMicrobiology/workshop0_2020-10-20
```

## Import ete3 and load a tree file

In either a python shell (e.g. iPython) or a python script we can 

```python
from ete3 import Tree
treeString = open('cluster_1_cf_scaled.tree','rt').read() 
t=Tree(treeString)
print(t)
```

Now instead of printing to a terminal, we're saving to a pdf file.

```python
from ete3 import Tree
treeString = open('cluster_1_cf_scaled.tree','rt').read() 
t=Tree(treeString)
t.render('figures/Staph_tree_1.pdf')
```

This should produce a file like the one below


![Alt-Text](figures/Staph_tree_1.png)


## Change node styles
I want to make the nodes invisible and make the lines a bit fatter.
Note here how we're importing TreeStyle and creating a function called mylayout.
Then how we add the tree style to the rendering.

```python
from ete3 import Tree, TreeStyle
treeString = open('cluster_1_cf_scaled.tree','rt').read()
t=Tree(treeString)

def mylayout(node):
    node.img_style["size"]=0
    node.img_style["vt_line_width"] = 2
    node.img_style["hz_line_width"] = 2

ts = TreeStyle()
ts.layout_fn = mylayout
t.render('figures/Staph_tree_2.png', tree_style=ts)
```

This should produce a file like the one below


![Alt-Text](figures/Staph_tree_2.png)


## Load some meta data and prune some leaves
I don't need all these samples for this paper. As some had different treatments.
So we're going to import pandas and load a metadata dataframe from a csv file.
We can create a list of leaves to keep, that are in the csv file, and use prune to keep only these.

```python
from ete3 import Tree, TreeStyle
import pandas as pd

# load tree
treeString = open('cluster_1_cf_scaled.tree','rt').read()
t=Tree(treeString)

# load some meta data
df=pd.read_csv('used_meta.csv')
keep_list=df['leaf'].to_list()
t.prune(keep_list)

# functions
def mylayout(node):
    node.img_style["size"]=0
    node.img_style["vt_line_width"] = 2
    node.img_style["hz_line_width"] = 2


# styles and rendering
ts = TreeStyle()
ts.layout_fn = mylayout
t.render('figures/Staph_tree_3.png', tree_style=ts)
```

This should produce a file like the one below


![Alt-Text](figures/Staph_tree_3.png)

## Add column
I want to show which samples had Staph cultured in the lab, to do this we're going to add some catagorical columns to show information. To do this we need to:

- [ ] import TextFace from ete3 
- [ ] define some colours to us. 
- [ ] add a cell function to add columns of information 
- [ ] Iterate over the leaves using a for loop to add meta data to the cell textface function 

```python
from ete3 import Tree, TreeStyle,TextFace
import pandas as pd

# load tree
treeString = open('cluster_1_cf_scaled.tree','rt').read()
t=Tree(treeString)

# load some meta data
df=pd.read_csv('used_meta.csv')
keep_list=df['leaf'].to_list()
t.prune(keep_list)

# colours
staph_pos_cols={True:'Green',
        False:'Red'}

# functions
def mylayout(node):
    node.img_style["size"]=0
    node.img_style["vt_line_width"] = 2
    node.img_style["hz_line_width"] = 2

def addCell(text,color):
    cell=TextFace(text)
    cell.background.color=color
    cell.margin_top = 5
    cell.margin_right = 5
    cell.margin_left = 5
    cell.margin_bottom = 5
    cell.opacity = 0.5 # from 0 to 1
    cell.border.width = 1
    return cell



# iterate over leaves
for lf in t.iter_leaves():
    leaf=lf.get_leaf_names(is_leaf_fn=None)[0]
    d=df[df['leaf']==leaf].to_dict(orient='records')
    # staph +ve 
    cell=addCell(d[0]['Staph culture'],
            staph_pos_cols[d[0]['Staph culture']])
    lf.add_face(cell, column=0, position="aligned")

# styles and rendering
ts = TreeStyle()
ts.layout_fn = mylayout
t.render('figures/Staph_tree_4.png', tree_style=ts)
```

This should produce a file like the one below


![Alt-Text](figures/Staph_tree_4.png)

## Remove leaf names and add a few more columns
Ok, now we can probably get rid of the leaf names and add that information into the columns, which might be more informative.

Here we are going to:
- [ ]  define some more colours to use
  - [ ] including importing from a json file because there are a lot of colours
  - [ ] create a heatmap of colours for the SNP distances

```python
from ete3 import Tree, TreeStyle,TextFace
import pandas as pd
import json

# load tree
treeString = open('cluster_1_cf_scaled.tree','rt').read()
t=Tree(treeString)

# load some meta data
df=pd.read_csv('used_meta.csv')
keep_list=df['leaf'].to_list()
t.prune(keep_list)
df['run number']=df['run number'].map(str)
df['patient']=df['patient'].map(str)

# colours
treatCol={'saponin 5%':'Khaki',
        'filter 5uM': 'DarkKhaki'}

staph_pos_cols={True:'Green',
        False:'Red'}

with open('colours.json', 'r') as fp:
    colours = json.load(fp)
    sampleCol=colours['sampleCols']
    runCol=colours['runCols']

def SNP_cols(n):
    if n==0:
        return 'White'
    elif n < 10:
        return 'Yellow'
    elif n < 50:
        return 'Orange'
    elif n < 200:
        return 'OrangeRed'
    elif n < 1000:
        return 'Red'
    elif n > 1000:
        return 'Maroon'
    else:
        return 'Dark grey'

# functions
def mylayout(node):
    node.img_style["size"]=0
    node.img_style["vt_line_width"] = 2
    node.img_style["hz_line_width"] = 2

def addCell(text,color):
    cell=TextFace(text)
    cell.background.color=color
    cell.margin_top = 5
    cell.margin_right = 5
    cell.margin_left = 5
    cell.margin_bottom = 5
    cell.opacity = 0.5 # from 0 to 1
    cell.border.width = 1
    return cell



# iterate over leaves
for lf in t.iter_leaves():
    leaf=lf.get_leaf_names(is_leaf_fn=None)[0]
    d=df[df['leaf']==leaf].to_dict(orient='records')
    # staph +ve
    cell=addCell(d[0]['Staph culture'],
            staph_pos_cols[d[0]['Staph culture']])
    lf.add_face(cell, column=0, position="aligned")
    # run number
    cell=addCell(d[0]['run number'],
            runCol[d[0]['run number']])
    lf.add_face(cell, column=1, position="aligned")
    # sample number
    cell=addCell(d[0]['patient'],
            sampleCol[d[0]['patient']])
    lf.add_face(cell, column=2, position="aligned")
    # sample number
    cell=addCell(d[0]['treatment'],
            treatCol[d[0]['treatment']])
    lf.add_face(cell, column=3, position="aligned")
    # Closest neighbour SNP number  
    cell=addCell(d[0]['SNPs'],
            SNP_cols(d[0]['SNPs']) )
    lf.add_face(cell, column=4, position="aligned")

# styles and rendering
ts = TreeStyle()
ts.show_leaf_name = False
ts.layout_fn = mylayout
t.render('figures/Staph_tree_5.png', tree_style=ts)
```

This should produce a file like the one below

![Alt-Text](figures/Staph_tree_5.png)
